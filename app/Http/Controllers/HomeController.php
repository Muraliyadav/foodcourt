<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\BookTable;

date_default_timezone_set("Asia/Kolkata");

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(request $request)
    {
        $table = BookTable::where('userid',Auth::user()->id)->get();
        return view('home',['table'=>$table]);
    }
    public function booktablestore(request $request){

         $data =new BookTable;
         $data->userid = Auth::user()->id;
         $data->name = $request->name;
         $data->email = $request->email;
         $data->number = $request->number;
         $data->fromtime = $request->from;
         $data->totime = $request->to;
         $data->date = $request->date;
         $data->pax = $request->pax;

         $data->type = $request->type;
         $data->hotel = $request->hotel;
         $data->status = "Booked";
         $data->save();
         return back();

    }
    public function gettabledeatils(request $request){

         $data = BookTable::where('id',$request->id)->get();

         return view('home',['data'=>$data]);
    }
}
