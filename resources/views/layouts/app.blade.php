<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Table Reservation') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('public/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <style>
.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: yellow;
   color: white;
   text-align: right;
}
</style>
</head>
<body>
  <!-- <a class="dropdown-item" href="{{ route('logout') }}"
     onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
      {{ __('Logout') }}
  </a> -->
    <div id="app" >


        <main class="py-4">
            @yield('content')
        </main>

    </div>

    <div>

      <!-- <a href="#"><span><i class="fa fa-bell">Github</i></span></a>
      <a href="#"><span><i class="fa fa-bell">Dribbble</i></span></a>
      <a href="#"><span><i class="fa fa-bell">CodePen</i> -->
        <ul class="navbar-nav ml-auto">
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        <div class="container">
                          <div class="footer row">
                            <!-- <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a> -->

                                <!-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"> -->

                                    <div class="col-3">
                                    <a href="{{URL::to('/')}}/home"> <i class="fa fa-home pull-center" style="font-size:48px;color:black"></i></a>
                                  </div>

                                  <div class="col-3">
                                  <a href="{{URL::to('/')}}/booktable"> <i class="fa fa-qrcode pull-center" style="font-size:48px;color:black"></i></a>
                                </div>
                                <div class="col-3">
                                <a href="{{URL::to('/')}}/booktable"> <i class="fa fa-plus-square pull-center" style="font-size:48px;color:black"></i></a>
                              </div>
                              <div class="col-3">

                                  <a class="dropdown-item pull-center" href="{{ route('logout') }}"
                                     onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                      <!-- {{ __('Logout') }} -->
                                      <i class="fa fa-sign-out" aria-hidden="true" style="font-size:48px;color:black"></i>
                                  </a>

                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>
                                </div>
                                <!-- </div> -->
                            </li>
                        @endguest
                    </ul>

      </span></a>
    </div>

  </div>

  </div>
</body>
</html>
