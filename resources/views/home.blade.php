
@extends('layouts.app')

@section('content')
<body style="background:limegreen">
<div class="container-fluid" >

    <div class="row justify-content-center">
        <div class="col-md-12">

                 <table class="table" style="font-size:10px;">
                    <thead style="background:lightgreen">
                        <th>#</th>
                        <th>Name</th>
                        <th>Pax</th>
                        <th>Elapsed Time</th>
                        <th>Date</th>
                        <th>Status</th>
                    </thead>
                     <tbody style="background:skyblue">
                        <?php $i=1; ?>
                        @foreach($table as $data)
                         <tr onclick="getdeatails('{{$data->id}}')" data-toggle="modal" data-target="#exampleModal{{$data->id}}">
                            <!-- Modal -->
<div class="modal fade" id="exampleModal{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Booked Table</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="card bg-success">
      <div class="card-body text-center">

        Name :        {{$data['name']}}<br>
        phone Numbe : {{$data['number']}}<br>
        Email(optional) : {{$data['email']}}<br>
        Booked Time :{{$data['fromtime']}}<br>
        Reserved Slot : {{$data['fromtime']}} to {{$data['totime']}}<br>
        Booked Date :{{$data['created_at']}}<br>
        Reserved Date : {{$data['date']}}<br>
        status : {{$data['status']}}<br>

 <div class="btn-group">
  <button type="button" class="btn btn-primary">Send Mail</button>
  <button type="button" class="btn btn-warning">Arrived</button>
  <button type="button" class="btn btn-info">Cancel</button>
</div>

      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

                            <td>{{$i++}}</td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->pax}}</td>
                            <td><?php
    $starttime = $data->fromtime;
    $stoptime = $data->totime;
    $diff = (strtotime($stoptime) - strtotime($starttime));
    $total = $diff/60;
    echo sprintf("%02dh %02dm", floor($total/60), $total%60);



                            ?>
                            </td>
                            <td>{{date('d-m-Y', strtotime($data->date))}}</td>
                            <td>{{$data->status}}</td>
                        </tr>
                        @endforeach
                     </tbody>
                 </table>
                <a href="{{URL::to('/')}}/booktable"> <i class="fa fa-plus-square pull-right" style="font-size:48px;color:black"></i></a>
                </div>
            </div>
          </div>
</body>
<script type="text/javascript">
    function getdeatails (argument) {
      var x = argument;
       $.ajax({
            type:'GET',
            url:"{{URL::to('/')}}/gettabledeatils",
            async:false,
            data:{id : x},
            success: function(response)
            {


                console.log(response);

               // setval(response);
             },
             error: function (error) {

                      console.log(error);

                    }
       });

    }
    // function setval(data){
    //   var months = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    //    var name = data[0]['name'];
    //    var number = data[0]['number'];
    //    var email = data[0]['email'];
    //    var booktime = data[0]['fromtime'];
    //    var Reservedslot = data[0]['totime'];
    //    let current_datetime = new Date(data[0]['created_at']);
    //    var bookeddate =current_datetime.getDate() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getFullYear();
    //     let current_datetime1 = new Date(data[0]['date']);
    //   var Reserveddate = current_datetime1.getDate() + "-" + months[current_datetime1.getMonth()] + "-" + current_datetime1.getFullYear();
    //   var status = data[0]['status'];

    // window.alert("hello javatpoint");



    // }
</script>
@endsection
