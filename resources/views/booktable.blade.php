@extends('layouts.app')
<style type="text/css">

body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: black;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  padding: 16px;
  background-color: white;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width:100%;
  padding: 8px;
  /* margin: 5px 0 22px 0; */
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}
</style>

@section('content')
<body style="background:limegreen">

<form action="{{URL::to('/')}}/booktablestore" method="post">

	{{csrf_field()}}
  <div class="container" style="font-size:8px">
    <!-- <h1 style="font-size:14px">Fill Details to Reserve Table</h1> -->

    <!-- <hr> -->
  <div class="form-group row">
    <label for="email" class="col-4 col-form-label "><b>Name</b></label>
    <div class="col-8">
    <input type="text" class="form-control" placeholder="Enter Name" name="name" required>
  </div>
</div>

<div class="form-group row">
    <label for="psw" class="col-4 col-form-label"><b>Phone No</b></label>
    <div class="col-8">
    <input type="text" class="form-control" placeholder="Enter Number" name="number" required>
  </div>
</div>

<div class="form-group row">
    <label for="psw-repeat" class="col-4 col-form-label"><b>Email</b></label>
      <div class="col-8">
    <input type="text" class="form-control" placeholder="Enter email" name="email" required>
  </div>
</div>
<!-- <label for="email" ><b>Reservation Booking Slot</b></label> -->

 <div class="form-group row">
       <div class="col-6">
       <label for="psw-repeat" class="col-2 col-form-label"><b>TimeFrom</b></label>
  <div class="col-10">
    <input type="time" class="form-control"  placeholder="Enter From" name="from" required>
  </div>
</div>
<div class="col-6">
  <label for="psw-repeat" class="col-2 col-form-label"><b>TimeTo</b></label>
  <div class="col-10">
    <input type="time" class="form-control" placeholder="Enter to" name="to" required>
</div>
</div>
</div>


<div class="form-group row">
  <div class="col-6">
  <label for="psw-repeat" class="col-2 col-form-label"><b>Date</b></label>
  <div class="col-10">
    <input type="date" class="form-control" placeholder="Enter date" name="date" required>
  </div>
</div>
<div class="col-6">
  <label for="psw-repeat" class="col-2 col-form-label"><b>Pax</b></label>
  <div class="col-10">
 <input type="text" class="form-control" placeholder="Occupied Seat" name="pax" required>

  </div>
</div>
</div>

<div class="form-group row">
  <div class="col-6">
  <label for="psw-repeat" class="col-2 col-form-label"><b>Type</b></label>
  <div class="col-10">
    <select class="form-control"  name="type">
    	<option>--select--</option>
    	<option value="single">Single</option>
    	<option value="group">Group</option>
    </select>
  </div>
</div>

  <div class="col-6">
  <label for="psw-repeat" class="col-2 col-form-label"><b>Hotel</b></label>
  <div class="col-10">
    <select class="form-control" name="hotel">
    	<option>--select--</option>
    	<option value="test">Test</option>
    	<option value="test1">Test</option>
    </select>
  </div>
</div>

</div>
    <button type="submit" class="registerbtn" >Register</button>
  </div>

</div>
</form>




@endsection
